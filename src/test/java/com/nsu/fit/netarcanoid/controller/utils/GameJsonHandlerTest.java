package com.nsu.fit.netarcanoid.controller.utils;

import com.nsu.fit.netarcanoid.model.objects.*;
import com.nsu.fit.netarcanoid.model.utils.*;
import org.json.simple.JSONObject;
import java.util.Iterator;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class GameJsonHandlerTest {
    @Test
    void getOpponentNicknameTest() {
        GameJsonHandler gameJsonHandler = new GameJsonHandler();
        JSONObject nicknameMessage = gameJsonHandler.createNicknameMessage("TestNickname");

        byte[] data = nicknameMessage.toJSONString().getBytes();
        String receiveData = new String(data, 0, data.length);
        assertEquals("TestNickname", gameJsonHandler.getOpponentNickname(receiveData));
    }

    @Test
    void getEnemyDirectionStr() {
        GameJsonHandler gameJsonHandler = new GameJsonHandler();
        Direction testDirection = Direction.STOP;
        JSONObject directionMessage = gameJsonHandler.createDirectionMessage(testDirection.toString());

        byte[] directionInBytes = directionMessage.toJSONString().getBytes();
        String receiveData = new String(directionInBytes, 0, directionInBytes.length);
        assertEquals("STOP", gameJsonHandler.getEnemyDirectionStr(receiveData));
    }

    @Test
    void getGameJson() {
        GameJsonHandler gameJsonHandler = new GameJsonHandler();

        Ball ball = new Ball(100.0, 100.0);
        Paddle ourPaddle = new Paddle(100.0, 50.0);
        Paddle enemyPaddle = new Paddle(100.0, 650.0);
        BrickCollection bricks = new BrickCollection();
        bricks.add(new Brick(100.0, 500.0));
        GameState gameState = new GameState();

        JSONObject resultJson = gameJsonHandler.getGameJson(ball, ourPaddle, enemyPaddle, bricks, gameState);

        byte[] resultInBytes = resultJson.toJSONString().getBytes();
        String receiveData = new String(resultInBytes, 0, resultInBytes.length);

        Ball receiveBall = gameJsonHandler.getBall(receiveData);
        assertEquals(100.0, receiveBall.getX());
        assertEquals(100.0, receiveBall.getY());

        Paddle receiveOurPaddle = gameJsonHandler.getPaddleOne(receiveData);
        assertEquals(100.0, receiveOurPaddle.getX());
        assertEquals(50.0, receiveOurPaddle.getY());

        Paddle receiveEnemyPaddle = gameJsonHandler.getPaddleTwo(receiveData);
        assertEquals(100.0, receiveEnemyPaddle.getX());
        assertEquals(650.0, receiveEnemyPaddle.getY());

        BrickCollection receiveBricks = gameJsonHandler.getBricks(receiveData);
        Iterator brickIter = receiveBricks.getIterator();
        if (brickIter.hasNext()) {
            Brick brick = (Brick) brickIter.next();
            assertEquals(100.0, brick.getX());
            assertEquals(500.0, brick.getY());
        }

        boolean isHostWin = gameJsonHandler.isHostWin(receiveData);
        assertFalse(isHostWin);

        boolean isUsualWin = gameJsonHandler.isUsualWin(receiveData);
        assertFalse(isUsualWin);
    }
}