package com.nsu.fit.netarcanoid.controller.utils;

import java.net.UnknownHostException;

import com.nsu.fit.netarcanoid.controller.utils.SearchJsonHandler;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.junit.jupiter.api.Test;
import java.net.DatagramPacket;
import java.net.InetAddress;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class SearchJsonHandlerTest {
    @Test
    void createSearchMessageTest() {
        SearchJsonHandler searchJsonHandler = new SearchJsonHandler();
        JSONObject message = searchJsonHandler.createSearchMessage("TestNickname");
        assertEquals("TestNickname", (String)message.get("PlayerNickname"));
    }

    @Test
    void getNicknameTest() throws UnknownHostException {
        SearchJsonHandler searchJsonHandler = new SearchJsonHandler();
        JSONObject message = searchJsonHandler.createSearchMessage("TestNickname");
        byte[] data = message.toJSONString().getBytes();
        DatagramPacket datagramPacket = new DatagramPacket(data, data.length, InetAddress.getByName("239.102.0.4"), 8888);
        String receiveData = new String(data, 0, datagramPacket.getLength());
        assertEquals("TestNickname", searchJsonHandler.getNickname(receiveData));
    }
}