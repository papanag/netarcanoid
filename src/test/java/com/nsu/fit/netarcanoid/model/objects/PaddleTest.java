package com.nsu.fit.netarcanoid.model.objects;

import java.lang.NumberFormatException;

import org.junit.jupiter.api.Test;

import static com.nsu.fit.netarcanoid.model.objects.Paddle.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class PaddleTest {

    @Test
    void getPaddleWidthTest() {
        assertEquals(60, getPaddleWidth());
    }

    @Test
    void getPaddleHeightTest() {
        assertEquals(30, getPaddleHeight());
    }

    @Test
    void getPaddleSpeedTest() { assertEquals(10, getPaddleSpeed()); }

    @Test
    void getXTest() {
        Paddle paddle = new Paddle(0.0, 0.0);
        assertEquals(0.0, paddle.getX());
    }

    @Test
    void getYTest() {
        Paddle paddle = new Paddle(0.0, 0.0);
        assertEquals(0.0, paddle.getY());
    }

    @Test
    void moveTest() {
        Paddle paddle = new Paddle(250.0, 0.0);
        assertEquals(250.0, paddle.getX());
        assertEquals(0.0, paddle.getY());
        paddle.trajectoryRight(450);
        paddle.move();
        assertEquals(260.0, paddle.getX());
        assertEquals(0.0, paddle.getY());
        paddle.trajectoryLeft();
        paddle.move();
        assertEquals(250.0, paddle.getX());
        assertEquals(0.0, paddle.getY());
        paddle.stop();
        paddle.move();
        assertEquals(250.0, paddle.getX());
        assertEquals(0.0, paddle.getY());
    }

    @Test
    void trajectoryLeftTest() throws NumberFormatException {
        Paddle firstPaddle = new Paddle(50, 50);
        firstPaddle.trajectoryLeft();
        firstPaddle.move();
        assertEquals(40, firstPaddle.getX());

        Paddle secondPaddle = new Paddle(30, 50);
        secondPaddle.trajectoryLeft();
        secondPaddle.move();
        assertEquals(30, secondPaddle.getX());
    }

    @Test
    void trajectoryRightTest() throws NumberFormatException {
        Paddle firstPaddle = new Paddle(50, 50);
        firstPaddle.trajectoryRight(450);
        firstPaddle.move();
        assertEquals(60, firstPaddle.getX());

        Paddle secondPaddle = new Paddle(420, 50);
        secondPaddle.trajectoryRight(450);
        secondPaddle.move();
        assertEquals(420, secondPaddle.getX());
    }

    @Test
    void stopTest() throws NumberFormatException {
        Paddle paddle = mock(Paddle.class);
        doThrow(new NumberFormatException()).when(paddle).stop();
    }

    @Test
    void getTopTest() {
        Paddle paddle = new Paddle(0.0, 0.0);
        assertEquals(0.0 - getPaddleHeight() / 2.0, paddle.getTop());
    }

    @Test
    void getBottomTest() {
        Paddle paddle = new Paddle(0.0, 0.0);
        assertEquals(0.0 + getPaddleHeight() / 2.0, paddle.getBottom());
    }

    @Test
    void getRightTest() {
        Paddle paddle = new Paddle(0.0, 0.0);
        assertEquals(0.0 + getPaddleWidth() / 2.0, paddle.getRight());
    }

    @Test
    void getLeftTest() {
        Paddle paddle = new Paddle(0.0, 0.0);
        assertEquals(0.0 - getPaddleWidth() / 2.0, paddle.getLeft());
    }
}