package com.nsu.fit.netarcanoid.model;

import java.util.Iterator;

import com.nsu.fit.netarcanoid.model.objects.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class ModelTest {
    @Test
    void modelWidthTest() {
        assertEquals(450, Model.getWIDTH());
    }

    @Test
    void modelHeightTest() {
        assertEquals(700, Model.getHEIGHT());
    }

    @Test
    void createBallInModelTest() {
        Model model = new Model();
        Ball ball = model.getBall();
        assertEquals(Model.getWIDTH() / 2.0, ball.getX());
        assertEquals(Model.getHEIGHT() - 100.0, ball.getY());
    }

    @Test
    void createOurPaddleInModelTest() {
        Model model = new Model();
        Paddle ourPaddle = model.getOurPaddle();
        assertEquals(Model.getWIDTH() / 2.0, ourPaddle.getX());
        assertEquals(Model.getHEIGHT() - 50.0, ourPaddle.getY());
    }

    @Test
    void createEnemyPaddleInModelTest() {
        Model model = new Model();
        Paddle enemyPaddle = model.getEnemyPaddle();
        assertEquals(Model.getWIDTH() / 2.0, enemyPaddle.getX());
        assertEquals(Model.getHEIGHT() - 650.0, enemyPaddle.getY());
    }

    @Test
    void createGameStateInModelTest() {
        Model model = new Model();
        GameState gameState = model.getGameState();
        assertFalse(gameState.isOurWin());
        assertFalse(gameState.isEnemyWin());
    }

    @Test
    void createBricksInModelTest() {
        Model model = new Model();
        BrickCollection bricks = model.getBricks();

        Iterator brickIterator = bricks.getIterator();
        Brick brick = (Brick) brickIterator.next();

        assertEquals(75, brick.getX());
        assertEquals(305, brick.getY());
    }

    @Test
    void ourPaddleRightTest() {
        Model model = new Model();
        model.ourPaddleRight();

        Paddle paddle = model.getOurPaddle();
        paddle.move();
        assertEquals(235, paddle.getX());
    }

    @Test
    void ourPaddleLeftTest() {
        Model model = new Model();
        model.ourPaddleLeft();

        Paddle paddle = model.getOurPaddle();
        paddle.move();
        assertEquals(215, paddle.getX());
    }

    @Test
    void ourPaddleStopTest() {
        Model model = new Model();
        model.ourPaddleLeft();
        model.ourPaddleStop();

        Paddle paddle = model.getOurPaddle();
        paddle.move();
        assertEquals(225, paddle.getX());
    }

    @Test
    void enemyPaddleRightTest() {
        Model model = new Model();
        model.enemyPaddleRight();

        Paddle paddle = model.getEnemyPaddle();
        paddle.move();
        assertEquals(235, paddle.getX());
    }

    @Test
    void enemyPaddleLeftTest() {
        Model model = new Model();
        model.enemyPaddleLeft();

        Paddle paddle = model.getEnemyPaddle();
        paddle.move();
        assertEquals(215, paddle.getX());
    }

    @Test
    void enemyPaddleStopTest() {
        Model model = new Model();
        model.enemyPaddleLeft();
        model.enemyPaddleStop();

        Paddle paddle = model.getEnemyPaddle();
        paddle.move();
        assertEquals(225, paddle.getX());
    }


    @Test
    void updateTest() {
        Model model = new Model();

        for (int i = 0; i < Model.getWIDTH() / 10 - 2; i++) {
            model.update();
        }
        assertEquals(Model.getWIDTH() - 10.0, model.getBall().getX());

        model.update();
        assertEquals(Model.getWIDTH() - 5.0, model.getBall().getX());

        model.update();
        assertEquals(Model.getWIDTH() - 10.0, model.getBall().getX());
    }

    @Test
    void observableTest() {
        Model model = new Model();

        Observer observer = mock(Observer.class);
        model.registerObserver(observer);

        Ball ball = model.getBall();
        Paddle ourPaddle = model.getOurPaddle();
        Paddle enemyPaddle = model.getEnemyPaddle();
        BrickCollection bricks = model.getBricks();

        model.notifyObservers(ball, ourPaddle, enemyPaddle, bricks);

        assertFalse(model.getGameState().isOurWin());
        assertFalse(model.getGameState().isEnemyWin());
    }
}
