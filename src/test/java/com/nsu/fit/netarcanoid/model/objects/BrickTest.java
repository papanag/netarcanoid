package com.nsu.fit.netarcanoid.model.objects;

import java.lang.NumberFormatException;

import org.junit.jupiter.api.Test;

import static com.nsu.fit.netarcanoid.model.objects.Brick.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class BrickTest {

    @Test
    void getBrickWidthTest() {
        assertEquals(60, getBrickWidth());
    }

    @Test
    void getBrickHeightTest() {
        assertEquals(30, getBrickHeight());
    }

    @Test
    void getXTest() {
        Brick brick = new Brick(0.0, 0.0);
        assertEquals(0.0, brick.getX());
    }

    @Test
    void getYTest() {
        Brick brick = new Brick(0.0, 0.0);
        assertEquals(0.0, brick.getY());
    }

    @Test
    void isDestroyedTest() {
        Brick brick = new Brick(0.0, 0.0);
        assertFalse(brick.isDestroyed());
        brick.setDestroyed(true);
        assertTrue(brick.isDestroyed());
    }

    @Test
    void setDestroyedTest() throws NumberFormatException {
        Brick brick = mock(Brick.class);
        doThrow(new NumberFormatException()).when(brick).setDestroyed(anyBoolean());
    }

    @Test
    void getTopTest() {
        Brick brick = new Brick(0.0, 0.0);
        assertEquals(0.0 - getBrickHeight() / 2.0, brick.getTop());
    }

    @Test
    void getBottomTest() {
        Brick brick = new Brick(0.0, 0.0);
        assertEquals(0.0 + getBrickHeight() / 2.0, brick.getBottom());
    }

    @Test
    void getRightTest() {
        Brick brick = new Brick(0.0, 0.0);
        assertEquals(0.0 + getBrickWidth() / 2.0, brick.getRight());
    }

    @Test
    void getLeftTest() {
        Brick brick = new Brick(0.0, 0.0);
        assertEquals(0.0 - getBrickWidth() / 2.0, brick.getLeft());
    }
}