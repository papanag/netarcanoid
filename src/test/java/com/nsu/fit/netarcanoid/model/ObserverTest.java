package com.nsu.fit.netarcanoid.model;

import java.lang.NumberFormatException;

import com.nsu.fit.netarcanoid.model.objects.Ball;
import com.nsu.fit.netarcanoid.model.objects.BrickCollection;
import com.nsu.fit.netarcanoid.model.objects.Paddle;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class ObserverTest {
    @Test
    void updateTest() throws NumberFormatException {
        Observer observer = mock(Observer.class);
        Ball ball = new Ball(0.0, 0.0);
        Paddle paddle1 = new Paddle(0.0, 0.0);
        Paddle paddle2 = new Paddle(0.0, 100.0);
        BrickCollection brickCollection = new BrickCollection();
        doThrow(new NumberFormatException()).when(observer).update(ball, paddle1, paddle2, brickCollection);
    }
}