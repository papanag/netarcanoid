package com.nsu.fit.netarcanoid.model.objects;

import java.lang.NumberFormatException;

import org.junit.jupiter.api.Test;

import static com.nsu.fit.netarcanoid.model.objects.BrickCollection.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class BrickCollectionTest {
    @Test
    void addTest() throws NumberFormatException {
        BrickCollection brickCollection = mock(BrickCollection.class);
        Brick brick = new Brick(0.0, 0.0);
        doThrow(new NumberFormatException()).when(brickCollection).add(brick);
    }
}