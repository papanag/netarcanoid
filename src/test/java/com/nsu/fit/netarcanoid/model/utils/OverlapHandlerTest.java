package com.nsu.fit.netarcanoid.model.utils;

import com.nsu.fit.netarcanoid.model.objects.Ball;
import com.nsu.fit.netarcanoid.model.objects.Brick;
import com.nsu.fit.netarcanoid.model.objects.GameState;
import com.nsu.fit.netarcanoid.model.objects.Paddle;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class OverlapHandlerTest {
    @Test
    void createHandlerTest() {
        OverlapHandler overlapHandler = new OverlapHandler(450, 700);

        assertEquals(450, overlapHandler.getWidth());
        assertEquals(700, overlapHandler.getHeight());
    }

    @Test
    void updateBallTest() {
        OverlapHandler overlapHandler = new OverlapHandler(450, 700);

        Ball firstBall = new Ball(5, 50);
        firstBall.setLeftTrajectory();
        GameState firstState = new GameState();
        overlapHandler.updateBall(firstBall, firstState);
        firstBall.move();
        assertEquals(5, firstBall.getX());

        Ball secondBall = new Ball(445, 50);
        GameState secondState = new GameState();
        overlapHandler.updateBall(secondBall, secondState);
        secondBall.move();
        assertEquals(445, secondBall.getX());

        Ball thirdBall = new Ball(225, 5);
        GameState thirdState = new GameState();
        overlapHandler.updateBall(thirdBall, thirdState);
        assertTrue(thirdState.isOurWin());

        Ball fourthBall = new Ball(225, 695);
        fourthBall.setUpTrajectory();
        GameState fourthState = new GameState();
        overlapHandler.updateBall(fourthBall, fourthState);
        assertTrue(fourthState.isEnemyWin());
    }

    @Test
    void handleBallAndOurPaddleTest() {
        OverlapHandler overlapHandler = new OverlapHandler(450, 700);
        Paddle paddle = new Paddle(225,50);

        /* left bound */
        Ball firstBall = new Ball(200, 50);
        overlapHandler.handleBallAndOurPaddleIntersection(paddle, firstBall);
        firstBall.move();
        assertEquals(195, firstBall.getX());
        assertEquals(45, firstBall.getY());

        /* right bound */
        Ball secondBall = new Ball(250, 50);
        Paddle secondPaddle = new Paddle(225, 50);
        overlapHandler.handleBallAndOurPaddleIntersection(paddle, secondBall);
        secondBall.move();
        assertEquals(255, secondBall.getX());
        assertEquals(45, secondBall.getY());

        /* no bound */
        Ball thirdBall = new Ball(50, 225);
        Paddle thirdPaddle = new Paddle(225, 50);
        overlapHandler.handleBallAndOurPaddleIntersection(paddle, thirdBall);
        thirdBall.move();
        assertEquals(55, thirdBall.getX());
        assertEquals(220, thirdBall.getY());
    }

    @Test
    void handleBallAndEnemyPaddleTest() {
        OverlapHandler overlapHandler = new OverlapHandler(450, 700);
        Paddle paddle = new Paddle(225, 50);

        /* right bound */
        Ball firstBall = new Ball(250, 55);
        overlapHandler.handleBallAndEnemyPaddleIntersection(paddle, firstBall);
        firstBall.move();
        assertEquals(255, firstBall.getX());
        assertEquals(60, firstBall.getY());

        /* left bound */
        Ball secondBall = new Ball(200, 55);
        overlapHandler.handleBallAndEnemyPaddleIntersection(paddle, secondBall);
        secondBall.move();
        assertEquals(195, secondBall.getX());
        assertEquals(60, secondBall.getY());

        /* no bound */
        Ball thirdBall = new Ball(225, 600);
        overlapHandler.handleBallAndEnemyPaddleIntersection(paddle, thirdBall);
        thirdBall.move();
        assertEquals(230, thirdBall.getX());
        assertEquals(595, thirdBall.getY());
    }

    @Test
    void handleBallAndBrickTest() {
        OverlapHandler overlapHandler = new OverlapHandler(450, 700);
        Brick brick = new Brick(225, 350);

        Ball firstBall = new Ball(220, 345);
        overlapHandler.handleBallAndBrickIntersection(brick, firstBall);
        firstBall.move();
        assertEquals(225, firstBall.getX());
        assertEquals(340, firstBall.getY());

        Ball secondBall = new Ball(220, 355);
        overlapHandler.handleBallAndBrickIntersection(brick, secondBall);
        secondBall.move();
        assertEquals(225, secondBall.getX());
        assertEquals(360, secondBall.getY());

        Ball thirdBall = new Ball(195, 350);
        overlapHandler.handleBallAndBrickIntersection(brick, thirdBall);
        thirdBall.move();
        assertEquals(190, thirdBall.getX());
        assertEquals(345, thirdBall.getY());

        Ball fourthBall = new Ball(255, 350);
        overlapHandler.handleBallAndBrickIntersection(brick, fourthBall);
        fourthBall.move();
        assertEquals(260, fourthBall.getX());
        assertEquals(345, fourthBall.getY());
    }
}
