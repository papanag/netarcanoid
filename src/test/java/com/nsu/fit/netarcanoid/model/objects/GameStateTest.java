package com.nsu.fit.netarcanoid.model.objects;

import java.lang.NumberFormatException;

import org.junit.jupiter.api.Test;

import static com.nsu.fit.netarcanoid.model.objects.GameState.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class GameStateTest {
    @Test
    void setOurWinTest() throws NumberFormatException {
        GameState gameState = new GameState();
        gameState.setOurWin(true);

        assertTrue(gameState.isOurWin());
    }

    @Test
    void isOurWinTest() {
        GameState gameState = new GameState();
        assertFalse(gameState.isOurWin());
    }

    @Test
    void setEnemyWinTest() throws NumberFormatException {
        GameState gameState = new GameState();
        gameState.setEnemyWin(true);

        assertTrue(gameState.isEnemyWin());
    }

    @Test
    void isEnemyWinTest() {
        GameState gameState = new GameState();
        assertFalse(gameState.isEnemyWin());
    }
}