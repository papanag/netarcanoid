package com.nsu.fit.netarcanoid.model.objects;

import java.lang.NumberFormatException;

import org.junit.jupiter.api.Test;

import static com.nsu.fit.netarcanoid.model.objects.Ball.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class BallTest {

    @Test
    void getBallRadiusTest() {
        assertEquals(10, getBallRadius());
    }

    @Test
    void getXTest() {
        Ball ball = new Ball(0.0, 0.0);
        assertEquals(0.0, ball.getX());
        ball.move();
        assertEquals(5.0, ball.getX());
    }

    @Test
    void getYTest() {
        Ball ball = new Ball(0.0, 0.0);
        assertEquals(0.0, ball.getY());
        ball.move();
        assertEquals(-5.0, ball.getY());
    }

    @Test
    void moveTest() {
        Ball ball = new Ball(0.0, 0.0);
        ball.move();
        assertEquals(5.0, ball.getX());
        assertEquals(-5.0, ball.getY());
        ball.setLeftTrajectory();
        ball.move();
        assertEquals(0.0, ball.getX());
        assertEquals(-10.0, ball.getY());
        ball.bounceOffEnemyPaddle();
        ball.move();
        assertEquals(-5.0, ball.getX());
        assertEquals(-5.0, ball.getY());
        ball.setRightTrajectory();
        ball.move();
        assertEquals(0.0, ball.getX());
        assertEquals(0.0, ball.getY());
        ball.bounceOffOurPaddle();
        ball.move();
        assertEquals(5.0, ball.getX());
        assertEquals(-5.0, ball.getY());
    }

    @Test
    void bounceOffOurPaddleTest() throws NumberFormatException {
        Ball ball = mock(Ball.class);
        doThrow(new NumberFormatException()).when(ball).bounceOffOurPaddle();
    }

    @Test
    void bounceOffEnemyPaddleTest() throws NumberFormatException {
        Ball ball = mock(Ball.class);
        doThrow(new NumberFormatException()).when(ball).bounceOffEnemyPaddle();
    }

    @Test
    void setUpTrajectoryTest() throws NumberFormatException {
        Ball ball = mock(Ball.class);
        doThrow(new NumberFormatException()).when(ball).setUpTrajectory();
    }

    @Test
    void setDownTrajectoryTest() throws NumberFormatException {
        Ball ball = mock(Ball.class);
        doThrow(new NumberFormatException()).when(ball).setDownTrajectory();
    }

    @Test
    void setRightTrajectoryTest() throws NumberFormatException {
        Ball ball = mock(Ball.class);
        doThrow(new NumberFormatException()).when(ball).setRightTrajectory();
    }

    @Test
    void setLeftTrajectoryTest() throws NumberFormatException {
        Ball ball = mock(Ball.class);
        doThrow(new NumberFormatException()).when(ball).setLeftTrajectory();
    }

    @Test
    void getTopTest() {
        Ball ball = new Ball(0.0, 0.0);
        assertEquals(0.0 - getBallRadius(), ball.getTop());
        ball.move();
        assertEquals(-5.0 - getBallRadius(), ball.getTop());
    }

    @Test
    void getBottomTest() {
        Ball ball = new Ball(0.0, 0.0);
        assertEquals(0.0 + getBallRadius(), ball.getBottom());
        ball.move();
        assertEquals(-5.0 + getBallRadius(), ball.getBottom());
    }

    @Test
    void getRightTest() {
        Ball ball = new Ball(0.0, 0.0);
        assertEquals(0.0 + getBallRadius(), ball.getRight());
        ball.move();
        assertEquals(5.0 + getBallRadius(), ball.getRight());
    }

    @Test
    void getLeftTest() {
        Ball ball = new Ball(0.0, 0.0);
        assertEquals(0.0 - getBallRadius(), ball.getLeft());
        ball.move();
        assertEquals(5.0 - getBallRadius(), ball.getLeft());
    }

    @Test
    void ballSpeedTest() {
        assertEquals(5, Ball.getBallSpeed());
    }
}