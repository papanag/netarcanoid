package com.nsu.fit.netarcanoid.view;

import com.nsu.fit.netarcanoid.view.utils.Status;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class StatusPanel extends JPanel implements ActionListener {
    private View  view;
    private JButton okButton;

    StatusPanel(View view, Status status) {
        this.view = view;

        setPreferredSize(new Dimension(450, 700));
        setBackground(Color.BLACK);

        GridBagLayout gbl = new GridBagLayout();
        GridBagConstraints gbc = new GridBagConstraints();
        setLayout(gbl);

        JLabel label = new JLabel(getStatusString(status));
        label.setFont(new Font(Font.DIALOG, Font.BOLD, 24));
        label.setForeground(Color.WHITE);
        label.setBackground(Color.BLACK);
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.anchor = GridBagConstraints.CENTER;
        gbl.setConstraints(label, gbc);
        add(label);

        okButton = new JButton("OK");
        okButton.setFont(new Font(Font.DIALOG, Font.BOLD, 16));
        okButton.addActionListener(this);
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.weightx = 1;
        gbc.weighty = 0;
        gbc.ipady = 5;
        gbc.fill = GridBagConstraints.BOTH;
        gbl.setConstraints(okButton, gbc);
        add(okButton);

        setVisible(true);
    }

    private String getStatusString(Status status) {
        switch (status) {
            case WIN:
                return "YOU WIN!";
            case LOSE:
                return "YOU LOSE!";
            default:
                return "TECH TROUBLE";
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(okButton)) {
            view.showStartPanel();
        }
    }
}
