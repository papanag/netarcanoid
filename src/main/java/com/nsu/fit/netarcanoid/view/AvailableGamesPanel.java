package com.nsu.fit.netarcanoid.view;

import com.nsu.fit.netarcanoid.controller.InitController;
import com.nsu.fit.netarcanoid.controller.MailService.SearchGameReceiver;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Set;

public class AvailableGamesPanel extends JPanel implements ActionListener {
    private transient InitController initController;
    private AvailableGamesContainer gamesContainer;

    private View view;
    private JButton backButton;

    AvailableGamesPanel(InitController initController, View view) {
        this.initController = initController;
        this.view = view;

        setPreferredSize(new Dimension(450, 700));
        setBackground(Color.BLACK);

        GridBagLayout gbl = new GridBagLayout();
        GridBagConstraints gbc = new GridBagConstraints();
        setLayout(gbl);

        gamesContainer = new AvailableGamesContainer();
        JScrollPane scrollPane = new JScrollPane(gamesContainer);
        scrollPane.setBorder(new LineBorder(Color.BLACK, 3));
        scrollPane.getViewport().setBackground(Color.BLACK);
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        gbc.anchor = GridBagConstraints.NORTH;
        gbl.setConstraints(scrollPane, gbc);
        add(scrollPane);

        backButton = new JButton("BACK");
        backButton.setFont(new Font(Font.DIALOG, Font.BOLD, 16));
        backButton.addActionListener(this);
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.weightx = 1;
        gbc.weighty = 0.02;
        gbc.fill = GridBagConstraints.BOTH;
        gbl.setConstraints(backButton, gbc);
        add(backButton);

        setVisible(true);
    }

    public void showGames(Set<SearchGameReceiver.PlayerInfo> playerInfoSet) {
        gamesContainer.clearContainer();
        for (SearchGameReceiver.PlayerInfo playerInfo : playerInfoSet) {
            gamesContainer.addPlayerInfo(playerInfo);
        }

        gamesContainer.update();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(backButton)) {
            initController.stopSearchGameReceiver();
            view.showStartPanel();
        }
    }


    private class AvailableGamesContainer extends JPanel implements Scrollable {
        private AvailableGamesContainer() {
            setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        }

        private void clearContainer() {
            for (Component component : getComponents()) {
                remove(component);
            }
        }

        private void addPlayerInfo(SearchGameReceiver.PlayerInfo playerInfo) {
            add(new PlayerInfoPanel(playerInfo));
        }

        private void update() {
            revalidate();
            repaint();
        }

        @Override
        public Dimension getPreferredScrollableViewportSize() {
            return new Dimension(450, 630);
        }

        @Override
        public int getScrollableUnitIncrement(Rectangle visibleRect, int orientation, int direction) {
            if (getComponentCount() > 0) {
                JComponent component = (JComponent) getComponents()[0];
                return component.getPreferredSize().height;
            }
            return 0;
        }

        @Override
        public int getScrollableBlockIncrement(Rectangle visibleRect, int orientation, int direction) {
            return (getComponentCount() > 0) ? 630 : 0;
        }

        @Override
        public boolean getScrollableTracksViewportWidth() {
            return true;
        }

        @Override
        public boolean getScrollableTracksViewportHeight() {
            return false;
        }
    }

    private class PlayerInfoPanel extends JPanel implements ActionListener {
        private String playerAddr;
        private JButton pickGameButton;

        private PlayerInfoPanel(SearchGameReceiver.PlayerInfo playerInfo) {
            this.playerAddr = playerInfo.getPlayerAddr();
            String playerNickname = playerInfo.getPlayerNickname();

            setLayout(new BorderLayout());

            JLabel infoLabel = new JLabel(" Player " + playerNickname);
            infoLabel.setFont(new Font(Font.DIALOG, Font.BOLD, 13));
            add(infoLabel, BorderLayout.WEST);

            pickGameButton = new JButton("PICK GAME");
            pickGameButton.addActionListener(this);
            pickGameButton.setFont(new Font(Font.DIALOG, Font.BOLD, 13));
            add(pickGameButton, BorderLayout.EAST);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource().equals(pickGameButton)) {
                initController.pickGame(playerAddr);
            }
        }
    }
}