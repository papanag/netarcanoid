package com.nsu.fit.netarcanoid.view;

import com.nsu.fit.netarcanoid.controller.InitController;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class EnterNicknamePanel extends JPanel {
    private transient InitController initController;

    private JTextField nicknameField;
    private JButton startGameButton;

    public EnterNicknamePanel(InitController initController) {
        this.initController = initController;
        setPreferredSize(new Dimension(450, 700));
        setBackground(Color.BLACK);

        GridBagLayout gbl = new GridBagLayout();
        GridBagConstraints gbc = new GridBagConstraints();
        setLayout(gbl);

        nicknameField = new JTextField(20);
        nicknameField.setText("ENTER NICKNAME");
        nicknameField.setFont(new Font(Font.DIALOG, Font.BOLD, 14));
        nicknameField.setForeground(Color.WHITE);
        nicknameField.setBackground(Color.BLACK);
        Border newBorder = BorderFactory.createMatteBorder(0,0,3,0, Color.WHITE);
        nicknameField.setBorder(newBorder);
        nicknameField.setHorizontalAlignment(JTextField.CENTER);
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbl.setConstraints(nicknameField, gbc);
        add(nicknameField);

        startGameButton = new JButton("START GAME");
        startGameButton.setFont(new Font(Font.DIALOG, Font.BOLD, 14));
        startGameButton.addActionListener(new StartGameListener());
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.insets = new Insets(15, 0, 0, 0);
        gbc.ipadx = 150;
        gbc.ipady = 5;
        gbl.setConstraints(startGameButton, gbc);
        add(startGameButton);

        setVisible(true);
    }

    public void emptyNickname() {
        String messageForUser = "Please, input nickname!";
        JOptionPane.showMessageDialog(this, messageForUser, "NO NICKNAME", JOptionPane.ERROR_MESSAGE);
    }

    private class StartGameListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource().equals(startGameButton)) {
                String nickname = nicknameField.getText();
                initController.start(nickname);
            }
        }
    }
}
