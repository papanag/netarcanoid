package com.nsu.fit.netarcanoid.view;

import com.nsu.fit.netarcanoid.controller.InitController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class StartSearchPanel extends JPanel implements ActionListener {
    private transient InitController initController;

    private View view;
    private JButton backButton;

    StartSearchPanel(InitController initController, View view) {
        this.initController = initController;
        this.view = view;

        setPreferredSize(new Dimension(450,700));
        setBackground(Color.BLACK);

        GridBagLayout gbl = new GridBagLayout();
        GridBagConstraints gbc = new GridBagConstraints();
        setLayout(gbl);

        JLabel label = new JLabel("SEARCHING OPPONENT");
        label.setFont(new Font(Font.DIALOG, Font.BOLD, 24));
        label.setForeground(Color.WHITE);
        label.setBackground(Color.BLACK);
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.anchor = GridBagConstraints.CENTER;
        gbl.setConstraints(label, gbc);
        add(label);

        backButton = new JButton("BACK");
        backButton.setFont(new Font(Font.DIALOG, Font.BOLD, 16));
        backButton.addActionListener(this);
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.weightx = 1;
        gbc.weighty = 0;
        gbc.ipady = 3;
        gbc.fill = GridBagConstraints.BOTH;
        gbl.setConstraints(backButton, gbc);
        add(backButton);

        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(backButton)) {
            initController.stopSearchGameSender();
            initController.stopHostController();
            view.showStartPanel();
        }
    }
}
