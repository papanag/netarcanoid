package com.nsu.fit.netarcanoid.view;

import com.nsu.fit.netarcanoid.controller.InitController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class StartPanel extends JPanel {
    private transient InitController initController;

    private JButton startGameButton;
    private JButton searchGameButton;

    public StartPanel(InitController initController) {
        this.initController = initController;

        setPreferredSize(new Dimension(450, 700));
        setBackground(Color.BLACK);

        GridBagLayout gbl = new GridBagLayout();
        GridBagConstraints gbc = new GridBagConstraints();
        setLayout(gbl);

        StartPanelListener startPanelListener = new StartPanelListener();

        startGameButton = new JButton("START GAME");
        startGameButton.addActionListener(startPanelListener);
        startGameButton.setFont(new Font(Font.DIALOG, Font.BOLD, 14));
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.ipadx = 160;
        gbc.ipady = 5;
        gbl.setConstraints(startGameButton, gbc);
        add(startGameButton);

        searchGameButton = new JButton("SEARCH GAME");
        searchGameButton.addActionListener(startPanelListener);
        searchGameButton.setFont(new Font(Font.DIALOG, Font.BOLD, 14));
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.insets = new Insets(15,0,0,0);
        gbc.ipadx = 150;
        gbc.ipady = 5;
        gbl.setConstraints(searchGameButton, gbc);
        add(searchGameButton);

        setVisible(true);
    }

    private class StartPanelListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource().equals(startGameButton)) {
                initController.startSearchGameSender();
            } else if (e.getSource().equals(searchGameButton)) {
                initController.startSearchGameReceiver();
            }
        }
    }
}