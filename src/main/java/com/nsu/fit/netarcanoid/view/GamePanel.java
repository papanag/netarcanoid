package com.nsu.fit.netarcanoid.view;

import com.nsu.fit.netarcanoid.controller.GameController;
import com.nsu.fit.netarcanoid.model.Observable;
import com.nsu.fit.netarcanoid.model.Observer;
import com.nsu.fit.netarcanoid.model.objects.Ball;
import com.nsu.fit.netarcanoid.model.objects.Brick;
import com.nsu.fit.netarcanoid.model.objects.BrickCollection;
import com.nsu.fit.netarcanoid.model.objects.Paddle;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Iterator;

public class GamePanel extends JPanel implements Observer {
    private transient GameController controller;

    private transient Ball ball = null;
    private transient Paddle playerOnePaddle = null;
    private transient Paddle playerTwoPaddle = null;
    private transient BrickCollection bricks = null;

    public GamePanel(GameController controller, Observable observable) {
        this.controller = controller;
        observable.registerObserver(this);

        setPreferredSize(new Dimension(450, 700));
        setBackground(Color.black);
        addKeyListener(new GameKeyListener());
        setFocusable(true);
        setVisible(true);
    }

    @Override
    public void update(Ball ball, Paddle playerOnePaddle, Paddle playerTwoPaddle, BrickCollection bricks) {
        this.ball = ball;
        this.playerOnePaddle = playerOnePaddle;
        this.playerTwoPaddle = playerTwoPaddle;
        this.bricks = bricks;

        repaint();
    }

    @Override
    public void paint(Graphics graphics) {
        super.paint(graphics);

        if (ball == null || playerOnePaddle == null || playerTwoPaddle == null ||bricks == null) return;

        graphics.setColor(Color.GREEN);
        int ballDiameter = Ball.getBallRadius() * 2;
        graphics.fillOval((int)ball.getLeft(), (int)ball.getTop(), ballDiameter, ballDiameter);

        graphics.setColor(Color.RED);
        graphics.fillRoundRect((int)playerOnePaddle.getLeft(), (int)playerOnePaddle.getTop(), Paddle.getPaddleWidth(), Paddle.getPaddleHeight(), 25, 25);

        graphics.setColor(Color.BLUE);
        graphics.fillRoundRect((int)playerTwoPaddle.getLeft(), (int)playerTwoPaddle.getTop(), Paddle.getPaddleWidth(), Paddle.getPaddleHeight(), 25, 25);

        graphics.setColor(Color.YELLOW);
        Iterator<Brick> iterator = bricks.getIterator();
        while (iterator.hasNext()) {
            Brick brick = iterator.next();
            graphics.fill3DRect((int)brick.getLeft(), (int)brick.getTop(), Brick.getBrickWidth(), Brick.getBrickHeight(), true);
        }
    }

    private class GameKeyListener extends KeyAdapter {
        @Override
        public void keyPressed(KeyEvent e) {
            controller.gameControl(e);
        }

        @Override
        public void keyReleased(KeyEvent e) {
            controller.stopMove();
        }
    }
}