package com.nsu.fit.netarcanoid.view;

import com.nsu.fit.netarcanoid.controller.GameController;
import com.nsu.fit.netarcanoid.controller.InitController;
import com.nsu.fit.netarcanoid.model.Observable;
import com.nsu.fit.netarcanoid.view.utils.Status;

import javax.swing.*;

public class View extends JFrame {
    private JPanel currentPanel = null;

    private EnterNicknamePanel enterNicknamePanel;
    private AvailableGamesPanel availableGamesPanel;

    private transient InitController initController;

    public View(InitController initController) {
        this.initController = initController;
        setFrameParameters();
    }

    private void setFrameParameters() {
        setTitle("ARCANOID GAME");
        setSize(450, 700 + 22);
        setResizable(false);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    /* ENTER NICKNAME */
    public void showEnterNicknamePanel() {
        enterNicknamePanel = new EnterNicknamePanel(initController);
        addPanel(enterNicknamePanel);
    }

    public void emptyNickname() {
        enterNicknamePanel.emptyNickname();
    }

    /* START */
    public void showStartPanel() {
        StartPanel startPanel = new StartPanel(initController);
        addPanel(startPanel);
    }

    /* START SEARCH OPPONENT */
    public void showStartSearchPanel() {
        StartSearchPanel startSearchPanel = new StartSearchPanel(initController, this);
        addPanel(startSearchPanel);
    }

    public void showAvailableGamesPanel() {
        availableGamesPanel = new AvailableGamesPanel(initController, this);
        addPanel(availableGamesPanel);
    }

    public void showWinPanel() {
        StatusPanel statusPanel = new StatusPanel(this, Status.WIN);
        addPanel(statusPanel);
    }

    public void showLosePanel() {
        StatusPanel statusPanel = new StatusPanel(this, Status.LOSE);
        addPanel(statusPanel);
    }

    public void showTechTroublePanel() {
        StatusPanel statusPanel = new StatusPanel(this, Status.TECH_TROUBLE);
        addPanel(statusPanel);
    }

    public AvailableGamesPanel getAvailableGamesPanel() {
        return availableGamesPanel;
    }

    /* GAME PANEL */
    public void showGamePanel(GameController gameController, Observable observable) {
        GamePanel gamePanel = new GamePanel(gameController, observable);
        addPanel(gamePanel);
    }

    private void addPanel(JPanel panel) {
        if (currentPanel != null) remove(currentPanel);

        currentPanel = panel;
        add(panel);
        setVisible(true);
        revalidate();
        repaint();
    }
}