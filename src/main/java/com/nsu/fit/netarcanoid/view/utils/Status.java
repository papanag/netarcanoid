package com.nsu.fit.netarcanoid.view.utils;

public enum Status {
    WIN,
    LOSE,
    TECH_TROUBLE
}
