package com.nsu.fit.netarcanoid.controller;

import com.nsu.fit.netarcanoid.controller.utils.GameJsonHandler;
import com.nsu.fit.netarcanoid.model.Model;
import com.nsu.fit.netarcanoid.model.objects.*;
import com.nsu.fit.netarcanoid.model.utils.Direction;
import com.nsu.fit.netarcanoid.view.View;
import org.json.simple.JSONObject;

import java.awt.event.KeyEvent;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.util.Date;
import java.util.logging.Logger;


public class HostController implements GameController, Runnable {
    private static final Logger LOGGER = Logger.getLogger(HostController.class.getName());
    private InitController initController;

    private DatagramSocket socket;
    private int sendPort = 7777;
    private int recvPort = 8888;
    private long timeToUpdate;

    private InetAddress opponentAddr;
    private String opponentNickname;
    private long lastOpponentTime;

    private Direction[] directions = {Direction.STOP, Direction.STOP};

    private Model model;
    private View view;

    private GameJsonHandler gameJsonHandler;

    public HostController(InitController initController, View view) throws IOException {
        this.initController = initController;
        this.view = view;

        socket = new DatagramSocket(recvPort);

        gameJsonHandler = new GameJsonHandler();
    }

    @Override
    public void run() {
        try {
            waitOpponentMessage();
            answerToOpponent();
            startGame();
        } catch (IOException e) {
            LOGGER.info(e.getMessage());
        } finally {
            closeAll();
        }
    }

    private void waitOpponentMessage() throws IOException {
        socket.setSoTimeout(1000);
        while (true) {
            if (Thread.currentThread().isInterrupted()) throw new IOException("INTERRUPTED");

            byte[] buffer = new byte[256];
            DatagramPacket datagramPacket = new DatagramPacket(buffer, buffer.length);

            try {
                socket.receive(datagramPacket);
            } catch (SocketTimeoutException e) {
                socket.setSoTimeout(1000);
                continue;
            }

            initController.stopSearchGameSender();

            String receiveData = new String(buffer, 0, datagramPacket.getLength());
            opponentAddr = datagramPacket.getAddress();
            opponentNickname = gameJsonHandler.getOpponentNickname(receiveData);
            return;
        }
    }

    private void answerToOpponent() throws IOException {
        String confirmMessage = opponentNickname;
        byte[] confirmMessageInBytes = confirmMessage.getBytes();

        DatagramPacket confirm = new DatagramPacket(confirmMessageInBytes, confirmMessageInBytes.length, opponentAddr, sendPort);
        socket.send(confirm);
    }

    private void startGame() throws IOException {
        model = new Model();
        initController.startGame(this, model);
        lastOpponentTime = new Date().getTime();

        int updatePerSecond = 30;
        setTimeout(1000 / updatePerSecond);
        while (!Thread.currentThread().isInterrupted()) {
            try {
                receiveMessage();
                updateTimeout();
            } catch (SocketTimeoutException e) {
                handleDirections();
                model.update();
                sendGame();
                checkEndGame();
                isTechTrouble();
                setTimeout(1000 / updatePerSecond);
            }
        }
    }

    private void receiveMessage() throws IOException {
        byte[] buffer = new byte[256];
        DatagramPacket datagramPacket = new DatagramPacket(buffer, buffer.length);

        socket.receive(datagramPacket);
        lastOpponentTime = new Date().getTime();

        String receiveData = new String(buffer, 0, datagramPacket.getLength());
        if (receiveData.equals(opponentNickname)) return;

        String enemyDirectionStr = gameJsonHandler.getEnemyDirectionStr(receiveData);
        Direction enemyDirection = Direction.valueOf(enemyDirectionStr);
        directions[1] = enemyDirection;
    }

    @Override
    public void gameControl(KeyEvent event) {
        int keyCode = event.getKeyCode();

        if (keyCode == KeyEvent.VK_RIGHT) directions[0] = Direction.RIGHT;
        else if (keyCode == KeyEvent.VK_LEFT) directions[0] = Direction.LEFT;
    }

    @Override
    public void stopMove() {
        directions[0] = Direction.STOP;
    }

    private void handleDirections() {
        Direction ourDirection = directions[0];
        switch (ourDirection) {
            case STOP:
                model.ourPaddleStop();
                break;
            case LEFT:
                model.ourPaddleLeft();
                break;
            case RIGHT:
                model.ourPaddleRight();
                break;
        }

        Direction enemyDirection = directions[1];
        switch (enemyDirection) {
            case STOP:
                model.enemyPaddleStop();
                break;
            case LEFT:
                model.enemyPaddleLeft();
                break;
            case RIGHT:
                model.enemyPaddleRight();
                break;
        }
    }

    private void checkEndGame() {
        GameState gameState = model.getGameState();

        if (gameState.isEnemyWin()) {
            view.showLosePanel();
            Thread.currentThread().interrupt();
        } else if (gameState.isOurWin()) {
            view.showWinPanel();
            Thread.currentThread().interrupt();
        }
    }

    private void sendGame() throws IOException {
        Ball ball = model.getBall();
        Paddle ourPaddle = model.getOurPaddle();
        Paddle enemyPaddle = model.getEnemyPaddle();
        BrickCollection bricks = model.getBricks();
        GameState gameState = model.getGameState();

        JSONObject resultJson = gameJsonHandler.getGameJson(ball, ourPaddle, enemyPaddle, bricks, gameState);
        byte[] gameInBytes = resultJson.toJSONString().getBytes();

        DatagramPacket datagramPacket = new DatagramPacket(gameInBytes, gameInBytes.length, opponentAddr, sendPort);
        socket.send(datagramPacket);
    }

    private void isTechTrouble() {
        long currentTime = new Date().getTime();

        if (currentTime - lastOpponentTime > 5000) {
            view.showTechTroublePanel();
            Thread.currentThread().interrupt();
        }
    }

    private void setTimeout(long time) throws IOException {
        timeToUpdate = new Date().getTime() + time;
        updateTimeout();
    }

    private void updateTimeout() throws IOException {
        int currentTimeout = (int)(timeToUpdate - new Date().getTime());
        if (currentTimeout <= 0) throw new SocketTimeoutException();

        socket.setSoTimeout(currentTimeout);
    }

    private void closeAll() {
        socket.close();
    }
}

