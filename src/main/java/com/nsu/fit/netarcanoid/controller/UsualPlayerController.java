package com.nsu.fit.netarcanoid.controller;

import com.nsu.fit.netarcanoid.controller.utils.GameJsonHandler;
import com.nsu.fit.netarcanoid.model.Observable;
import com.nsu.fit.netarcanoid.model.Observer;
import com.nsu.fit.netarcanoid.model.objects.Ball;
import com.nsu.fit.netarcanoid.model.objects.BrickCollection;
import com.nsu.fit.netarcanoid.model.objects.Paddle;
import com.nsu.fit.netarcanoid.model.utils.Direction;
import com.nsu.fit.netarcanoid.view.View;
import org.json.simple.JSONObject;

import java.awt.event.KeyEvent;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.logging.Logger;


public class UsualPlayerController implements Observable, GameController, Runnable {
    private static final Logger LOGGER = Logger.getLogger(UsualPlayerController.class.getName());
    private InitController initController;

    private DatagramSocket socket;
    private int sendPort = 8888;
    private int recvPort = 7777;

    private InetAddress opponentAddr;
    private String ourNickname;

    private View view;
    private ArrayList<Observer> observers = new ArrayList<>();

    private GameJsonHandler gameJsonHandler;

    public UsualPlayerController(String opponentAddr, String ourNickname, InitController initController, View view) throws IOException {
        this.initController = initController;
        this.view = view;

        this.opponentAddr = InetAddress.getByName(opponentAddr);
        this.ourNickname = ourNickname;
        socket = new DatagramSocket(recvPort);

        gameJsonHandler = new GameJsonHandler();
    }

    @Override
    public void run() {
        try {
            writeToOpponent();
            getConfirmMessage();
            initController.startGame(this, this);

            while (!Thread.currentThread().isInterrupted()) {
                readGameState();
                lifeSignal();
            }
        } catch (IOException e) {
            LOGGER.info(e.getMessage());
        } finally {
            closeAll();
        }
    }

    private void writeToOpponent() throws IOException {
        JSONObject nicknameMessage = gameJsonHandler.createNicknameMessage(ourNickname);

        byte[] data = nicknameMessage.toJSONString().getBytes();
        DatagramPacket datagramPacket = new DatagramPacket(data, data.length, opponentAddr, sendPort);
        socket.send(datagramPacket);
    }

    private void getConfirmMessage() throws IOException {
        socket.setSoTimeout(3000);
        try {
            byte[] receiveData = new byte[256];
            DatagramPacket datagramPacket = new DatagramPacket(receiveData, receiveData.length);

            socket.receive(datagramPacket);

            String receiveStr = new String(receiveData, 0, datagramPacket.getLength());
            if (!receiveStr.equals(ourNickname)) Thread.currentThread().interrupt();
        } catch (SocketTimeoutException e) {
            view.showTechTroublePanel();
            throw new IOException("GG INTERRUPT");
        }
    }

    private void readGameState() throws IOException {
        byte[] gameStateInBytes = new byte[8192];
        DatagramPacket datagramPacket = new DatagramPacket(gameStateInBytes, gameStateInBytes.length);

        socket.setSoTimeout(2000);
        try {
            socket.receive(datagramPacket);
        } catch (SocketTimeoutException e) {
            view.showTechTroublePanel();
            throw new IOException();
        }

        String gameStateStr = new String(gameStateInBytes, 0, datagramPacket.getLength());

        boolean isHostWin = gameJsonHandler.isHostWin(gameStateStr);
        boolean isUsualWin = gameJsonHandler.isUsualWin(gameStateStr);

        if (isUsualWin) {
            view.showWinPanel();
            Thread.currentThread().interrupt();
            return;
        } else if (isHostWin) {
            view.showLosePanel();
            Thread.currentThread().interrupt();
            return;
        }

        Ball ball = gameJsonHandler.getBall(gameStateStr);
        Paddle paddleOne = gameJsonHandler.getPaddleOne(gameStateStr);
        Paddle paddleTwo = gameJsonHandler.getPaddleTwo(gameStateStr);
        BrickCollection bricks = gameJsonHandler.getBricks(gameStateStr);

        notifyObservers(ball, paddleOne, paddleTwo, bricks);
    }

    @Override
    public void gameControl(KeyEvent event) {
        int keyCode = event.getKeyCode();
        Direction ourDirection = Direction.STOP;

        if (keyCode == KeyEvent.VK_RIGHT) ourDirection = Direction.RIGHT;
        else if (keyCode == KeyEvent.VK_LEFT) ourDirection = Direction.LEFT;

        try {
            JSONObject jsonObject = gameJsonHandler.createDirectionMessage(ourDirection.toString());

            byte[] directionInBytes = jsonObject.toJSONString().getBytes();
            DatagramPacket datagramPacket = new DatagramPacket(directionInBytes, directionInBytes.length, opponentAddr, sendPort);
            socket.send(datagramPacket);
        } catch (IOException e) {
            LOGGER.info(e.getMessage());
        }
    }

    @Override
    public void stopMove() {
        try {
            JSONObject jsonObject = gameJsonHandler.createDirectionMessage(Direction.STOP.toString());

            byte[] directionInBytes = jsonObject.toJSONString().getBytes();
            DatagramPacket datagramPacket = new DatagramPacket(directionInBytes, directionInBytes.length, opponentAddr, sendPort);
            socket.send(datagramPacket);
        } catch (IOException e) {
            LOGGER.info(e.getMessage());
        }
    }

    private void lifeSignal() throws IOException {
        byte[] signalInBytes = ourNickname.getBytes();
        DatagramPacket datagramPacket = new DatagramPacket(signalInBytes, signalInBytes.length, opponentAddr, sendPort);

        socket.send(datagramPacket);
    }

    @Override
    public void registerObserver(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void notifyObservers(Ball ball, Paddle playerOnePaddle, Paddle playerTwoPaddle, BrickCollection bricks) {
        for (Observer observer : observers) {
            observer.update(ball, playerOnePaddle, playerTwoPaddle, bricks);
        }
    }

    private void closeAll() {
        socket.close();
    }
}
