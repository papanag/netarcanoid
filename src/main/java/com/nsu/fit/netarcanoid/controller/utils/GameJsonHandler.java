package com.nsu.fit.netarcanoid.controller.utils;

import com.nsu.fit.netarcanoid.model.objects.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.util.Iterator;

public class GameJsonHandler {
    /* Host controller */
    public String getOpponentNickname(String receiveData) {
        JSONObject jsonObject = (JSONObject) JSONValue.parse(receiveData);
        return (String)jsonObject.get("PlayerNickname");
    }

    public String getEnemyDirectionStr(String receiveData) {
        JSONObject jsonObject = (JSONObject) JSONValue.parse(receiveData);
        return (String) jsonObject.get("Direction");
    }

    public JSONObject getGameJson(Ball ball, Paddle ourPaddle, Paddle enemyPaddle, BrickCollection bricks, GameState gameState) {
        JSONObject resultJson = new JSONObject();

        resultJson.put("Ball", getJsonBall(ball));
        resultJson.put("OurPaddle", getPaddleJson(ourPaddle));
        resultJson.put("EnemyPaddle", getPaddleJson(enemyPaddle));
        resultJson.put("Bricks", getBricksJsonArray(bricks));
        resultJson.put("HostWin", gameState.isOurWin());
        resultJson.put("UsualWin", gameState.isEnemyWin());

        return resultJson;
    }

    private JSONObject getJsonBall(Ball ball) {
        JSONObject ballJson = new JSONObject();
        ballJson.put("X", ball.getX());
        ballJson.put("Y", ball.getY());

        return ballJson;
    }

    private JSONObject getPaddleJson(Paddle paddle) {
        JSONObject paddleJson = new JSONObject();
        paddleJson.put("X", paddle.getX());
        paddleJson.put("Y", paddle.getY());

        return paddleJson;
    }

    private JSONArray getBricksJsonArray(BrickCollection bricks) {
        JSONArray bricksJson = new JSONArray();
        Iterator<Brick> iterator = bricks.getIterator();

        while (iterator.hasNext()) {
            Brick brick = iterator.next();

            JSONObject brickJson = new JSONObject();
            brickJson.put("X", brick.getX());
            brickJson.put("Y", brick.getY());

            bricksJson.add(brickJson);
        }

        return bricksJson;
    }

    /* Usual player controller */
    public JSONObject createNicknameMessage(String nickname) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("PlayerNickname", nickname);

        return jsonObject;
    }

    public boolean isHostWin(String receiveData) {
        JSONObject jsonObject = (JSONObject) JSONValue.parse(receiveData);
        return (boolean)jsonObject.get("HostWin");
    }

    public boolean isUsualWin(String receiveData) {
        JSONObject jsonObject = (JSONObject) JSONValue.parse(receiveData);
        return (boolean) jsonObject.get("UsualWin");
    }

    public Ball getBall(String receiveData) {
        JSONObject jsonObject = (JSONObject) JSONValue.parse(receiveData);

        JSONObject ballJson = (JSONObject)jsonObject.get("Ball");
        double ballJsonX = (double) ballJson.get("X");
        double ballJsonY = (double) ballJson.get("Y");

        return new Ball(ballJsonX, ballJsonY);
    }

    public Paddle getPaddleOne(String receiveData) {
        JSONObject jsonObject = (JSONObject) JSONValue.parse(receiveData);

        JSONObject paddleOneJson = (JSONObject)jsonObject.get("OurPaddle");
        double paddleOneX = (double) paddleOneJson.get("X");
        double paddleOneY = (double) paddleOneJson.get("Y");

        return new Paddle(paddleOneX, paddleOneY);
    }

    public Paddle getPaddleTwo(String receiveData) {
        JSONObject jsonObject = (JSONObject) JSONValue.parse(receiveData);

        JSONObject paddleTwoJson = (JSONObject) jsonObject.get("EnemyPaddle");
        double paddleTwoX = (double) paddleTwoJson.get("X");
        double paddleTwoY = (double) paddleTwoJson.get("Y");

        return new Paddle(paddleTwoX, paddleTwoY);
    }

    public BrickCollection getBricks(String receiveData) {
        JSONObject jsonObject = (JSONObject) JSONValue.parse(receiveData);

        JSONArray bricksJson = (JSONArray)jsonObject.get("Bricks");
        BrickCollection brickCollection = new BrickCollection();

        for (Object object : bricksJson) {
            JSONObject brickJson = (JSONObject) object;
            double brickJsonX = (double) brickJson.get("X");
            double brickJsonY = (double) brickJson.get("Y");

            brickCollection.add(new Brick(brickJsonX, brickJsonY));
        }

        return brickCollection;
    }

    public JSONObject createDirectionMessage(String directionStr) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("Direction", directionStr);

        return jsonObject;
    }
}
