package com.nsu.fit.netarcanoid.controller.MailService;

import com.nsu.fit.netarcanoid.controller.utils.SearchJsonHandler;
import com.nsu.fit.netarcanoid.view.AvailableGamesPanel;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.SocketTimeoutException;
import java.util.*;
import java.util.logging.Logger;

public class SearchGameReceiver implements Runnable {
    private static final Logger LOGGER = Logger.getLogger(SearchGameReceiver.class.getName());
    private String playerNickname;
    private HashMap<PlayerInfo, Long> connectionList;

    private long timeToUpdate;

    private int port = 9999;
    private MulticastSocket multicastSocket;
    private SearchJsonHandler searchJsonHandler;

    private AvailableGamesPanel gamesPanel;


    public SearchGameReceiver(String playerNickname, AvailableGamesPanel gamesPanel) throws IOException {
        this.playerNickname = playerNickname;
        this.connectionList = new HashMap<>();

        this.gamesPanel = gamesPanel;

        multicastSocket = new MulticastSocket(port);
        multicastSocket.joinGroup(InetAddress.getByName("239.102.0.4"));

        searchJsonHandler = new SearchJsonHandler();
    }

    @Override
    public void run() {
        try {
            setTimeout(2000);
            while (!Thread.interrupted()) {
                mainCycle();
            }
        } catch (IOException e) {
            LOGGER.info(e.getMessage());
        } finally {
            closeAll();
        }
    }

    private void mainCycle() throws IOException {
        try {
            receiveMessage();
            updateTimeout();
        } catch (SocketTimeoutException e) {
            deleteOldMessages();
            gamesPanel.showGames(connectionList.keySet());
            setTimeout(2000);
        }
    }

    private void receiveMessage() throws IOException {
        byte[] buffer = new byte[256];
        DatagramPacket datagramPacket = new DatagramPacket(buffer, buffer.length);

        multicastSocket.receive(datagramPacket);
        String receiveData = new String(buffer, 0, datagramPacket.getLength());

        String playerAddr = datagramPacket.getAddress().getHostAddress();
        String playerName = searchJsonHandler.getNickname(receiveData);
        addPlayerInfo(playerAddr, playerName);
    }

    private void addPlayerInfo(String playerAddr, String playerNickname) {
        /* ищем информацию о том, присылал ли уже данный игрок сообщение */
        Set<PlayerInfo> playerInfoSet = connectionList.keySet();

        for (PlayerInfo playerInfo : playerInfoSet) {
            if (playerInfo.getPlayerAddr().equals(playerAddr)) {
                /* Обновляем информацию о времени последнего сообщения */
                connectionList.put(playerInfo, new Date().getTime());
                return;
            }
        }

        /* Если такой игрок не отправлял нам сообщения до этого, то добавляем его */
        connectionList.put(new PlayerInfo(playerAddr, playerNickname), new Date().getTime());
    }

    private void deleteOldMessages() {
        Iterator<Map.Entry<PlayerInfo, Long>> iterator = connectionList.entrySet().iterator();

        while (iterator.hasNext()) {
            Map.Entry<PlayerInfo, Long> entry = iterator.next();
            long lastMessageTime = entry.getValue();

            /* Если от пользователя 4 секунды не было сообщений - удаляем его */
            if (new Date().getTime() - lastMessageTime > 4000) {
                iterator.remove();
            }
        }
    }

    private void setTimeout(long time) throws IOException {
        timeToUpdate = new Date().getTime() + time;
        updateTimeout();
    }

    private void updateTimeout() throws IOException {
        int currentTimeout = (int)(timeToUpdate - new Date().getTime());
        if (currentTimeout <= 0) throw new SocketTimeoutException();

        multicastSocket.setSoTimeout(currentTimeout);
    }

    private void closeAll() {
        multicastSocket.close();
    }

    /*
     * Класс отвечает за хранение информации об других игроках,
     * которые расслылают сообщение о поиске противника
     */
    public class PlayerInfo {
        private String playerAddr;
        private String playerNickname;

        PlayerInfo(String playerAddr, String playerName) {
            this.playerAddr = playerAddr;
            this.playerNickname = playerName;
        }

        public String getPlayerAddr() {
            return playerAddr;
        }

        public String getPlayerNickname() {
            return playerNickname;
        }
    }
}
