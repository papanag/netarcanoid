package com.nsu.fit.netarcanoid.controller;

import java.awt.event.KeyEvent;

public interface GameController {
    void gameControl(KeyEvent e);
    void stopMove();
}
