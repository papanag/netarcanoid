package com.nsu.fit.netarcanoid.controller.utils;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

public class SearchJsonHandler {
    public JSONObject createSearchMessage(String playerNickname) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("PlayerNickname", playerNickname);

        return jsonObject;
    }

    public String getNickname(String receiveData) {
        JSONObject jsonObject = (JSONObject) JSONValue.parse(receiveData);
        return (String)jsonObject.get("PlayerNickname");
    }
}
