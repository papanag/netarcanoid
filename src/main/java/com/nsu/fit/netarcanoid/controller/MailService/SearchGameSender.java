package com.nsu.fit.netarcanoid.controller.MailService;

import com.nsu.fit.netarcanoid.controller.utils.SearchJsonHandler;
import org.json.simple.JSONObject;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.logging.Logger;

public class SearchGameSender implements Runnable {
    private static final Logger LOGGER = Logger.getLogger(SearchGameSender.class.getName());
    private String playerNickname;

    private int port = 9999;
    private InetAddress multicastAddr;
    private MulticastSocket multicastSocket;

    private SearchJsonHandler searchJsonHandler;

    public SearchGameSender(String playerNickname) throws IOException {
        this.playerNickname = playerNickname;

        multicastSocket = new MulticastSocket(port);
        multicastAddr = InetAddress.getByName("239.102.0.4");

        multicastSocket.joinGroup(multicastAddr);

        searchJsonHandler = new SearchJsonHandler();
    }

    @Override
    public void run() {
        try {
            while (!Thread.interrupted()) {
                sendMessage();
                Thread.sleep(2000);
            }
        } catch (IOException | InterruptedException e) {
            LOGGER.info(e.getMessage());
            Thread.currentThread().interrupt();
        } finally {
            closeAll();
        }
    }

    private void sendMessage() throws IOException {
        JSONObject gameInfoJson = searchJsonHandler.createSearchMessage(playerNickname);
        byte[] gameInfoJsonBytes = gameInfoJson.toJSONString().getBytes();

        DatagramPacket datagramPacket = new DatagramPacket(gameInfoJsonBytes, gameInfoJsonBytes.length, multicastAddr, port);
        multicastSocket.send(datagramPacket);
    }

    private void closeAll() {
        multicastSocket.close();
    }
}
