package com.nsu.fit.netarcanoid.controller;

import com.nsu.fit.netarcanoid.controller.MailService.SearchGameReceiver;
import com.nsu.fit.netarcanoid.controller.MailService.SearchGameSender;
import com.nsu.fit.netarcanoid.model.Observable;
import com.nsu.fit.netarcanoid.view.View;

import java.io.IOException;
import java.util.logging.Logger;

public class InitController {
    private static final Logger LOGGER = Logger.getLogger(InitController.class.getName());

    private Thread searchGameSender;
    private Thread hostControllerThread;
    private Thread searchGameReceiver;
    private String playerNickname;

    private View view;

    public void init() {
        view = new View(this);
        view.showEnterNicknamePanel();
    }

    public void start(String playerNickname) {
        if (playerNickname.isEmpty()) {
            view.emptyNickname();
            return;
        }

        this.playerNickname = playerNickname;
        view.showStartPanel();
    }

    public void startSearchGameSender() {
        view.showStartSearchPanel();

        try {
            searchGameSender = new Thread(new SearchGameSender(playerNickname));
            hostControllerThread = new Thread(new HostController(this, view));

            searchGameSender.start();
            hostControllerThread.start();
        } catch (IOException e) {
            LOGGER.info(e.getMessage());
        }
    }

    public void stopSearchGameSender() {
        if (searchGameSender != null) {
            searchGameSender.interrupt();
        }

        searchGameSender = null;
    }

    public void stopHostController() {
        if (hostControllerThread != null) {
            hostControllerThread.interrupt();
        }

        hostControllerThread = null;
    }


    public void startSearchGameReceiver() {
        view.showAvailableGamesPanel();

        try {
            searchGameReceiver = new Thread(new SearchGameReceiver(playerNickname, view.getAvailableGamesPanel()));
            searchGameReceiver.start();
        } catch (IOException e) {
            LOGGER.info(e.getMessage());
        }
    }

    public void stopSearchGameReceiver() {
        if (searchGameReceiver != null) {
            searchGameReceiver.interrupt();
        }

        searchGameReceiver = null;
    }

    public void pickGame(String playerAddr) {
        stopSearchGameReceiver();

        try {
            Thread usualControllerThread = new Thread(new UsualPlayerController(playerAddr, playerNickname, this, view));
            usualControllerThread.start();
        } catch (IOException e) {
            LOGGER.info(e.getMessage());
        }
    }

    void startGame(GameController gameController, Observable observable) {
        view.showGamePanel(gameController, observable);
    }
}
