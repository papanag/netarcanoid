package com.nsu.fit.netarcanoid.model;

import com.nsu.fit.netarcanoid.model.objects.Ball;
import com.nsu.fit.netarcanoid.model.objects.BrickCollection;
import com.nsu.fit.netarcanoid.model.objects.Paddle;

public interface Observer {
    public void update(Ball ball, Paddle playerOnePaddle, Paddle playerTwoPaddle, BrickCollection bricks);
}
