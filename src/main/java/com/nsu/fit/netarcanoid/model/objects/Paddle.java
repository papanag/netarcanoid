package com.nsu.fit.netarcanoid.model.objects;


public class Paddle implements BaseDrawObject {
    private static final int PADDLE_WIDTH = 60;
    private static final int PADDLE_HEIGHT = 30;
    private static final double PADDLE_SPEED = 10;

    public static int getPaddleWidth() {
        return PADDLE_WIDTH;
    }

    public static int getPaddleHeight() {
        return PADDLE_HEIGHT;
    }

    public static double getPaddleSpeed() {
        return PADDLE_SPEED;
    }

    private double x;
    private double y;

    private double xTrajectory;

    public Paddle(double x, double y) {
        this.x = x;
        this.y = y;

        xTrajectory = 0.0; // изначально панель неподвижна
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public void move() {
        x += xTrajectory;
    }

    public void trajectoryLeft() {
        xTrajectory = getLeft() > 0.0 ? -PADDLE_SPEED : 0.0;
    }

    public void trajectoryRight(int gameFieldWidth) {
        xTrajectory = getRight() < gameFieldWidth ? PADDLE_SPEED : 0.0;
    }

    public void stop() {
        xTrajectory = 0.0;
    }

    @Override
    public double getTop() {
        return y - PADDLE_HEIGHT / 2.0;
    }

    @Override
    public double getBottom() {
        return y + PADDLE_HEIGHT / 2.0;
    }

    @Override
    public double getRight() {
        return x + PADDLE_WIDTH / 2.0;
    }

    @Override
    public double getLeft() {
        return x - PADDLE_WIDTH / 2.0;
    }
}
