package com.nsu.fit.netarcanoid.model.utils;

import java.io.Serializable;

public enum Direction implements Serializable {
    LEFT,
    RIGHT,
    STOP
}
