package com.nsu.fit.netarcanoid.model.objects;

public class Brick implements BaseDrawObject {
    private static final int BRICK_WIDTH = 60;
    private static final int BRICK_HEIGHT = 30;

    public static int getBrickWidth() {
        return BRICK_WIDTH;
    }

    public static int getBrickHeight() {
        return BRICK_HEIGHT;
    }

    private boolean destroyed = false;

    private double x;
    private double y;

    public Brick(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public boolean isDestroyed() {
        return destroyed;
    }

    public void setDestroyed(boolean destroyed) {
        this.destroyed = destroyed;
    }

    @Override
    public double getTop() {
        return y - BRICK_HEIGHT / 2.0;
    }

    @Override
    public double getBottom() {
        return y + BRICK_HEIGHT / 2.0;
    }

    @Override
    public double getRight() {
        return x + BRICK_WIDTH / 2.0;
    }

    @Override
    public double getLeft() {
        return x - BRICK_WIDTH / 2.0;
    }


}
