package com.nsu.fit.netarcanoid.model.objects;

public class Ball implements BaseDrawObject {
    private static final int BALL_RADIUS = 10;
    private static final double BALL_SPEED = 5; // скорость передвижения в условных единицах за 1 тик

    public static int getBallRadius() {
        return BALL_RADIUS;
    }

    public static double getBallSpeed() {
        return BALL_SPEED;
    }

    /* координаты центра мяча */
    private double x;
    private double y;

    /* направо вверх */
    private double xTrajectory =  BALL_SPEED;
    private double yTrajectory = -BALL_SPEED;

    /* Координаты центра */
    public Ball(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public void move() {
        x += xTrajectory;
        y += yTrajectory;
    }

    /* Отражаем "вниз" (вниз - т.к. ось y направлена вниз) */
    public void bounceOffOurPaddle() {
        setDownTrajectory();
    }

    public void bounceOffEnemyPaddle() {
        setUpTrajectory();
    }

    public void setUpTrajectory() {
        yTrajectory = BALL_SPEED; // тк ось y направлена вниз
    }

    public void setDownTrajectory() {
        yTrajectory = -BALL_SPEED;
    }

    public void setRightTrajectory() {
        xTrajectory = BALL_SPEED;
    }

    public void setLeftTrajectory() {
        xTrajectory = -BALL_SPEED;
    }

    @Override
    public double getTop() {
        return y - BALL_RADIUS; // ось y идет с верхнего левого угла вниз
    }

    @Override
    public double getBottom() {
        return y + BALL_RADIUS;
    }

    @Override
    public double getRight() {
        return x + BALL_RADIUS;
    }

    @Override
    public double getLeft() {
        return x - BALL_RADIUS;
    }
}
