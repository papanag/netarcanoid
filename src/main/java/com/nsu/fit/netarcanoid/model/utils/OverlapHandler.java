package com.nsu.fit.netarcanoid.model.utils;

import com.nsu.fit.netarcanoid.model.objects.*;

public class OverlapHandler {
    private int width;
    private int height;

    public OverlapHandler(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public void updateBall(Ball ball, GameState gameState) {
        ball.move();

        if (ball.getLeft() < 0)
            ball.setRightTrajectory();
        else if (ball.getRight() > width)
            ball.setLeftTrajectory();

        if (ball.getTop() < 0)
            gameState.setOurWin(true);
        else if (ball.getBottom() > height)
            gameState.setEnemyWin(true);
    }

    public void handleBallAndOurPaddleIntersection(Paddle ourPaddle, Ball ball) {
        if (!isOverlap(ourPaddle, ball))
            return;

        ball.bounceOffOurPaddle();

        if (ball.getX() < ourPaddle.getX())
            ball.setLeftTrajectory();
        else
            ball.setRightTrajectory();
    }

    public void handleBallAndEnemyPaddleIntersection(Paddle enemyPaddle, Ball ball) {
        if (!isOverlap(enemyPaddle, ball)) return;

        ball.bounceOffEnemyPaddle();

        if (ball.getX() < enemyPaddle.getX())
            ball.setLeftTrajectory();
        else
            ball.setRightTrajectory();
    }


    public void handleBallAndBrickIntersection(Brick brick, Ball ball) {
        if (!isOverlap(brick, ball)) return;

        brick.setDestroyed(true);

        double overlapLeft = ball.getRight() - brick.getLeft();
        double overlapRight = brick.getRight() - ball.getLeft();
        double overlapTop = ball.getBottom() - brick.getTop();
        double overlapBottom = brick.getBottom() - ball.getTop();

        boolean ballFromLeft = overlapLeft < overlapRight;
        boolean ballFromTop = overlapTop < overlapBottom;

        double minOverlapX = ballFromLeft ? overlapLeft : overlapRight;
        double minOverlapY = ballFromTop ? overlapTop : overlapBottom;

        if (minOverlapX < minOverlapY) {
            if (ballFromLeft)
                ball.setLeftTrajectory();
            else
                ball.setRightTrajectory();
        } else {
            if (ballFromTop)
                ball.setDownTrajectory();
            else
                ball.setUpTrajectory();
        }
    }

    private boolean isOverlap(BaseDrawObject first, BaseDrawObject second) {
        return first.getRight() >= second.getLeft() && first.getLeft() <= second.getRight() &&
                first.getBottom() >= second.getTop() && first.getTop() <= second.getBottom();
    }
}
