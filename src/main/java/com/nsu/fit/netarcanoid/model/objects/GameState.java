package com.nsu.fit.netarcanoid.model.objects;

public class GameState {
    private boolean ourWin = false;
    private boolean enemyWin = false;

    public void setOurWin(boolean ourWin) {
        this.ourWin = ourWin;
    }

    public boolean isOurWin() {
        return ourWin;
    }

    public void setEnemyWin(boolean enemyWin) {
        this.enemyWin = enemyWin;
    }

    public boolean isEnemyWin() {
        return enemyWin;
    }
}