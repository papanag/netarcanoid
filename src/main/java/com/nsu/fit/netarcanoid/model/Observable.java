package com.nsu.fit.netarcanoid.model;

import com.nsu.fit.netarcanoid.model.objects.Ball;
import com.nsu.fit.netarcanoid.model.objects.BrickCollection;
import com.nsu.fit.netarcanoid.model.objects.Paddle;

public interface Observable {
    void registerObserver(Observer observer);
    void notifyObservers(Ball ball, Paddle playerOnePaddle, Paddle playerTwoPaddle, BrickCollection bricks);
}
