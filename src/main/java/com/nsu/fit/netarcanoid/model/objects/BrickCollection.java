package com.nsu.fit.netarcanoid.model.objects;

import java.util.ArrayList;
import java.util.Iterator;

public class BrickCollection {
    private ArrayList<Brick> bricks;

    public BrickCollection() {
        bricks = new ArrayList<>();
    }

    public void add(Brick brick) {
        bricks.add(brick);
    }

    public Iterator<Brick> getIterator() {
        return bricks.iterator();
    }
}