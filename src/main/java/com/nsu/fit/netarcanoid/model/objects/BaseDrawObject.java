package com.nsu.fit.netarcanoid.model.objects;

public interface BaseDrawObject {
    double getTop();
    double getBottom();
    double getRight();
    double getLeft();
}
