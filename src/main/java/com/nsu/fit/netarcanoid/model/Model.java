package com.nsu.fit.netarcanoid.model;
import com.nsu.fit.netarcanoid.model.objects.*;
import com.nsu.fit.netarcanoid.model.utils.OverlapHandler;

import java.util.ArrayList;
import java.util.Iterator;

public class Model implements Observable {
    private static final int WIDTH = 450; // длина поля в условных единицах
    private static final int HEIGHT = 700; // высота поля в условных единицах

    private static final int BLOCK_COUNT_X = 6; // количество блоков по горизонтали
    private static final int BLOCK_COUNT_Y = 4; // количество блоков по вертикали

    private ArrayList<Observer> observers = new ArrayList<>();

    public static int getWIDTH() {
        return WIDTH;
    }

    public static int getHEIGHT() {
        return HEIGHT;
    }

    private Ball ball;
    private Paddle ourPaddle;
    private Paddle enemyPaddle;
    private BrickCollection bricks;

    private GameState gameState;
    private OverlapHandler overlapHandler;

    public Model() {
        ball = new Ball(WIDTH / 2.0, HEIGHT - 100.0);
        ourPaddle = new Paddle(WIDTH / 2.0, HEIGHT - 50.0);
        enemyPaddle = new Paddle(WIDTH / 2.0, HEIGHT - 650.0);
        initBricks();

        gameState = new GameState();
        overlapHandler = new OverlapHandler(WIDTH, HEIGHT);
    }

    private void initBricks() {
        bricks = new BrickCollection();

        int xShift = (WIDTH - BLOCK_COUNT_X * Brick.getBrickWidth()) / 2;
        int yShift = (HEIGHT - BLOCK_COUNT_Y * Brick.getBrickHeight()) / 2;

        for (int i = 0; i < BLOCK_COUNT_X; i++) {
            for (int j = 0; j < BLOCK_COUNT_Y; j++) {
                double brickCenterX = xShift + i * Brick.getBrickWidth() + Brick.getBrickWidth() / 2.0;
                double brickCenterY = yShift + j * Brick.getBrickHeight() + Brick.getBrickHeight() / 2.0;
                Brick brick = new Brick(brickCenterX, brickCenterY);
                bricks.add(brick);
            }
        }
    }

    public Ball getBall() {
        return ball;
    }

    public Paddle getOurPaddle() {
        return ourPaddle;
    }

    public Paddle getEnemyPaddle() {
        return enemyPaddle;
    }

    public BrickCollection getBricks() {
        return bricks;
    }

    public GameState getGameState() {
        return gameState;
    }

    public void update() {
        overlapHandler.updateBall(ball, gameState);
        updateOurPaddle();
        updateEnemyPaddle();

        overlapHandler.handleBallAndOurPaddleIntersection(ourPaddle, ball);
        overlapHandler.handleBallAndEnemyPaddleIntersection(enemyPaddle, ball);

        Iterator<Brick> iterator = bricks.getIterator();
        while (iterator.hasNext()) {
            Brick brick = iterator.next();
            overlapHandler.handleBallAndBrickIntersection(brick, ball);

            if (brick.isDestroyed()) iterator.remove();
        }

        notifyObservers(ball, ourPaddle, enemyPaddle, bricks);
    }

    private void updateOurPaddle() {
        ourPaddle.move();
    }

    public void ourPaddleRight() {
        ourPaddle.trajectoryRight(WIDTH);
    }

    public void ourPaddleLeft() {
        ourPaddle.trajectoryLeft();
    }

    public void ourPaddleStop() {
        ourPaddle.stop();
    }


    private void updateEnemyPaddle() {
        enemyPaddle.move();
    }

    public void enemyPaddleRight() {
        enemyPaddle.trajectoryRight(WIDTH);
    }

    public void enemyPaddleLeft() {
        enemyPaddle.trajectoryLeft();
    }

    public void enemyPaddleStop() {
        enemyPaddle.stop();
    }

    @Override
    public void registerObserver(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void notifyObservers(Ball ball, Paddle playerOnePaddle, Paddle playerTwoPaddle, BrickCollection bricks) {
        for (Observer observer : observers) {
            observer.update(ball, playerOnePaddle, playerTwoPaddle, bricks);
        }
    }
}