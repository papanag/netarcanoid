[![Статус Порога Качества](http://84.237.50.237:9000/api/project_badges/measure?project=arc%3Aarcanoid&metric=alert_status)](http://84.237.50.237:9000/dashboard?id=arc%3Aarcanoid)
## NETARCANOID
### Участники
>Лусников Василий 17208 - Developer

>Зиануров Нурсултан 17208 - Developer

>Ромахин Илья 17206 - Developer

### Используемые инструменты

1. Тестовое покрытие определяется с помощью `OpenClover` (Была ошибка между плагинами jacoco + surefire)
    - [посмотреть можно тут](https://papanag.gitlab.io/netarcanoid/)
 
1.  Для всего остального 
    - [SonarQube](http://84.237.50.237:9000/dashboard?id=arc%3Aarcanoid)
3. JavaDoc 
    - [посмотерть можно тут](https://papanag.gitlab.io/netarcanoid/)
3. Для показа сайтов
    - Gitlab Pages 